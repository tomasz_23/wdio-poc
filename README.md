Requirements:
 - node >= 12

Command for install packages and dependencies to run locally: 
 - `npm install`

Run tests:
 - `npx wdio run ./apps.conf.ts --suite example --Denv=APPSQA`

 - for multisession -> `npx wdio run ./apps.conf.ts --suite example --Denv=APPSQA --Dmultisession=true`

Run static analizye of code:
 - `npx eslint test/**/*.ts`

Report:
 - install allure-comandline locally on machine using 
 <br> `npm i allure-commandline`
 - in terminal 
    1. `cd target`
    2. `allure serve`

 Docs:
 https://webdriver.io/docs/api/browser/waitUntil/