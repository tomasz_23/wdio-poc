var devVariable = {
    baseUrl: 'https://su-appsqa.appsqa-astack-tumorboard.hi5.platform.navify.com/',
    supportToolUrl: 'https://appsqa-astack-support.hi5.platform.navify.com/',

    params: {
        login: {
            email: 'enterpriseuser@yahoo.com',
            password: 'Foundation1!2@3#',
            name: 'enterprise user',
            oktaid: '00u5ynz227SgZfuEG297',
            facility: 'SUPPORT',
            facilityID: 'ce5f164c-ae0f-11e7-9220-c7137b83fb6a',
            tenantAlias: 'su-appsqa',
            role: 'CUSTOMER_ADMIN'
        },
        loginUCSF: {
            email: 'damian.zalewski+vou-su-appsqa@contractors.roche.com',
            password: 'Foundation1!2@3#',
            name: 'Viewonly User',
            oktaid: '00u8vku6gpzeZ5emD297',
            facility: 'SUPPORT',
            facilityID: 'ce5f164c-ae0f-11e7-9220-c7137b83fb6a',
            tenantAlias: 'su-appsqa',
            role: 'VIEW_ONLY_USER'
        },
        loginCorrectAddress: {
            email: 'damian.zalewski+ca-sh-appsqa@contractors.roche.com',
            password: 'Foundation1!2@3#',
            name: 'Correct Address',
            oktaid: '00u8t8xyt1LNvToV4297',
            facility: 'Summa Health',
            facilityID: 'f9483a7a-ab12-4276-9ed0-e8fbf086eba8',
            tenantAlias: 'sh-appsqa',
            role: 'CUSTOMER_ADMIN'
        },
        loginAppsDefaultUser: {
            email: 'damian.zalewski+du-su-appsqa@contractors.roche.com',
            password: 'Foundation1!2@3#',
            name: 'Enterprise Default user',
            oktaid: '00u8vkyhwdC8PA4ut297',
            facility: 'SUPPORT',
            facilityID: 'ce5f164c-ae0f-11e7-9220-c7137b83fb6a',
            tenantAlias: 'su-appsqa',
            role: 'DEFAULT_USER'
        },
        loginDisabledApps: {
            email: 'damian.zalewski+ca-da-appsqa@contractors.roche.com',
            password: 'Foundation1!2@3#',
            name: 'Admin User',
            oktaid: '00u8vl0jilZ995Cci297',
            facility: 'Disabled Apps',
            facilityID: '29f4f83b-161f-4566-bd0d-bc27ba209dfd',
            tenantAlias: 'da-appsqa',
            role: 'CUSTOMER_ADMIN'
        },
    }
};
module.exports = devVariable;