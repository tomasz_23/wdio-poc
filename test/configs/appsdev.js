var devVariable = {
    baseUrl: 'https://su-appsdev.appsdev-astack-tumorboard.hi5.platform.navify.com/',
    supportToolUrl: 'https://appsdev-astack-support.hi5.platform.navify.com/',

    params: {
        login: {
            email: 'enterpriseuser@yahoo.com',
            password: 'Foundation1!2@3#',
            name: 'enterprise user',
            oktaid: '00u4o6bbvAeNZkXKL296',
            facility: 'SUPPORT',
            facilityID: 'ce5f164c-ae0f-11e7-9220-c7137b83fb6a',
            tenantAlias: 'su-appsdev',
            role: 'CUSTOMER_ADMIN'
        },
        loginUCSF: {
            email: 'damian.zalewski+vou-su-appsdev@contractors.roche.com',
            password: 'Foundation1!2@3#',
            name: 'Viewonly User',
            oktaid: '00u8vku6gpzeZ5emD297',
            facility: 'SUPPORT',
            facilityID: 'ce5f164c-ae0f-11e7-9220-c7137b83fb6a',
            tenantAlias: 'su-appsdev',
            role: 'VIEW_ONLY_USER'
        },
        loginAppsDefaultUser: {
            email: 'damian.zalewski+du-su-appsdev@contractors.roche.com',
            password: 'Foundation1!2@3#',
            name: 'Default User',
            oktaid: '00u8vkyhwdC8PA4ut297',
            facility: 'SUPPORT',
            facilityID: 'ce5f164c-ae0f-11e7-9220-c7137b83fb6a',
            tenantAlias: 'su-appsdev',
            role: 'DEFAULT_USER'
        },
        loginCorrectAddress: {
            email: 'damian.zalewski+ca-sh-appsdev@contractors.roche.com',
            password: 'Foundation1!2@3#',
            name: 'Correct Address',
            oktaid: '00u9168j4wpFJrdCp297',
            facility: 'Summa Health',
            facilityID: 'dd646eda-f3b3-4637-bfba-012e8e698b84',
            tenantAlias: 'sh-appsdev',
            role: 'CUSTOMER_ADMIN'
        },
        loginDisabledApps: {
            email: 'damian.zalewski+ca-da-appsdev@contractors.roche.com',
            password: 'Foundation1!2@3#',
            name: 'Admin User',
            oktaid: '00u916a5nu9b0eUB9297',
            facility: 'Disabled Apps',
            facilityID: 'c4f3f3e0-c079-452a-bc9a-5533d74e1887',
            tenantAlias: 'da-appsdev',
            role: 'CUSTOMER_ADMIN'
        },
    }
};
module.exports = devVariable;