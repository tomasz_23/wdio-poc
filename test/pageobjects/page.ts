import logger from "@wdio/logger";

const { textToBePresentInElement } = require('wdio-wait-for');

export default class Page {
    readonly timeoutValue = 15000;
    readonly timeoutValueShort = 6000;
    protected logger=logger('[UI]');
    
    constructor(browser = null) {
        this._browser = browser;
    }

    get browser() {
        return this._browser ? this._browser : browser;
    }

    protected getText(locator: $): string {
        locator.waitForDisplayed();
        return locator.getText();
    }

    protected click(locator: $): void {
        locator.waitForClickable({
            timeouut: 30,
            timeoutMsg: 'Element is not clickable',
            interval: 3
        });
        locator.moveTo();
        locator.click();
    }

    protected getFrames(): any {
        return $$('iframe');
    }

    protected switchOnCurrentFrame() {
        browser.pause(5000);
        browser.switchToFrame(this.getFrames()[0]);
    }

    protected waitForElementisInvisible(locator: $): void {
        locator.waitForDisplayed();
        browser.waitUntil(() => !locator.isDisplayed(), { timeout: this.timeoutValue, timeoutMsg: 'Element is still displayed' });
    }

    protected selectElementByValue(list: $$, value: string): any {
        return list.find(element => element.getText().includes(value))
    }

    protected waitForElementContainsText(locator: $, value: string): void {
        browser.waitUntil(() => textToBePresentInElement(locator, value),
            {
                timeout: this.timeoutValue,
                timeoutMsg: `locator doesn't have text: ${value}`
            });
    }

    protected waitAndClickIfDisplayed(locator: $): void {
        try {
            browser.waitUntil(() => locator.isDisplayed(),
                { timeout: this.timeoutValueShort, interval: 2000 });
            this.click(locator);
        } catch (error) {
            this.logger.warn(locator.error.message)
        }
    }
}
