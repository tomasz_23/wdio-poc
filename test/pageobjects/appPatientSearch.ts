import Page from "./page";

export class AppPatientSearch extends Page {
    get component() { return this.browser.$('app-patient-search') }
    get input() { return this.component.$('ng-select input') }
    get options() { return this.component.$$('.patient-search__option') }

    select(searchText: string, fullText: any = null) {
        this.input.waitForDisplayed();
        this.input.setValue(searchText);

        browser.waitUntil(() => this.options.length > 0, { timeout: 10000, timeoutMsg: 'List is empty' })

        if (fullText) {
            const elements: any = this.options.map(element => element.getText().includes(fullText));
            this.click(elements[0]);
        } else this.click(this.options[0]);

        browser.pause(5000);
    }
}