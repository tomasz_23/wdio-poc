
import { Guideline } from "../../utils/enums/Guideline";
import Page from "../page";

export class GuidelinesPage extends Page {
    get component() { return this.browser.$('gdl-generic-view') }

    get helpAndMore() { return this.component.$('.mat-button--help') }
    get analytics() { return this.browser.$('.mat-menu-content [routerlink="/analytics"]') }

    get sourceSelectBox() { return this.component.$('[label="Guideline Source:"]') }
    get optionsSource() { return this.browser.$$('.mat-select-panel-wrap mat-option') }

    get cancerTypeSelectBox() { return this.component.$('[label="Cancer Type:"]') }
    get optionsCancer() { return this.browser.$$('.ps-content .mat-menu-item') }

    get startingPointSelectBox() { return this.component.$('[label="Starting Point:"]') }
    get optionsStartingPoints() { return this.browser.$$('.ps-content mat-option') }

    get title() { return this.component.$('.title') }

    get stepView() { return this.component.$('[routerlink="step"]') }
    get treeView() { return this.component.$('[routerlink="tree"]') }
    get graphZoom() { return this.component.$('gdl-graph-zoom') }

    get clinicalPathwayList() { return this.component.$$('.gdl-clinical-path button span') }

    get gdlNodes() { return this.component.$('gdl-nodes-list , .nodes') }
    get pathContainers() { return this.gdlNodes.$$('gdl-content') }

    get reset() { return this.component.$('[data-mat-icon-name="reset"]') }
    get print() { return this.component.$('[mattooltip="Print"] mat-icon') }

    get closeDialogVersionButton() { return this.browser.$('mat-dialog-container button') }
    get principles() { return this.component.$('.principles') }

    public fillDropDownList(dropdownlist: Guideline, value: T): any {
        switch (dropdownlist) {
            case Guideline.GuidelineSource:
                this.click(this.sourceSelectBox);
                this.click(this.selectElementByValue(this.optionsSource, value));
                break;

            case Guideline.GuidelineCancerType:
                this.click(this.cancerTypeSelectBox);
                this.click(this.selectElementByValue(this.optionsCancer, value));
                this.waitAndClickIfDisplayed(this.closeDialogVersionButton);
                this.waitForElementContainsText(this.title, value);
                this.principles.waitForDisplayed();
                break;

            case Guideline.GuidelineStartingPoint:
                this.click(this.startingPointSelectBox);
                this.click(this.selectElementByValue(this.optionsStartingPoints, value));
                break;

            default:
                break;
        }
    }

    public selectTreeView(): void {
        this.click(this.treeView);
        this.graphZoom.waitForDisplayed();
    }

    public selectAndGetNode(index = 0): string {
        this.gdlNodes.waitForDisplayed();
        const pathWayValue = this.getText(this.pathContainers[index].$('p'));
        this.click(this.pathContainers[index]);
        this.reset.waitForDisplayed();
        this.waitForElementContainsText(this.clinicalPathwayList[index], pathWayValue);
        return pathWayValue;
    }

    public goToAnalytics(): void {
        this.click(this.helpAndMore);
        this.click(this.analytics);
    }
}