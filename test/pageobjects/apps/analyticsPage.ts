import { Customization } from "../../utils/enums/Customization";
import Page from "../page";

export class AnalyticsPage extends Page {
    get component() { return $('div.gdl-analytics') }

    get barChart() { return this.component.$('gdl-bar-chart') }
    get gdlAnalyticsPathwaysByCancerListElements() { return $$('(//*[name()="svg"]//*[@class="yAxis"])[1]//*[@class="tick"]//*[name()="text"]') }

    get rect() { return this.component.$$('.layer .rect') }
    get tooltip() { return $('.analytics .d3js-tooltip') }

    private getCancerTypeInChartIndex(cancerType: string): number {
        return this.gdlAnalyticsPathwaysByCancerListElements.findIndex(element =>
            element.getAttribute('data-test').includes(cancerType)
        ) + 1;
    }

    public hoverOnColumn(cancerType: string, customization: Customization): void {
        const index = this.getCancerTypeInChartIndex(cancerType);
        const column = $(`(//*[@class='layer'])[${customization}]/*[@class='rect'][${index}]`);
        column.click();
    }

    public getAmountOfCancerTypeFromTooltip(): string {
        const value = this.tooltip.getText();
        return value.substring(value.indexOf('customization: (') + 16, value.length - 1);
    }
}