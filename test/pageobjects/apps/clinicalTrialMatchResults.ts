import Page from "../page";

export class ClinicalTrialMatchResults extends Page {
    get component() { return this.browser.$('nap-search-results') }
    get emptyState() { return this.component.$('nap-empty-state') }
    get matchList() { return this.component.$('nap-clinical-trials-list-item').$$('h2') }
}
