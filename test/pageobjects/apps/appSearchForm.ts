import Page from "../page";
import { ClinicalTrialMatchResults } from "./clinicalTrialMatchResults";

export class AppSearchForm extends Page {
    get component() { return this.browser.$('nap-search-form') }
    get searchField() { return this.component.$('input') }
    get loader() { return this.browser.$('nap-spinner') }

    get helpMoreButton() { return this.component.$('header__help').$('button') }
    get printButton() { return this.component.$('.print-button') }

    public search(value: string, isResult = true): void {
        this.searchField.setValue(value);
        browser.keys("Enter");

        this.waitForElementisInvisible(this.loader);
        if (isResult) { new ClinicalTrialMatchResults(this.browser).matchList[0].waitForDisplayed(); }
    }

    public clickPrintButton() {
        this.click(this.printButton);
        browser.pause(15000);
    }
}