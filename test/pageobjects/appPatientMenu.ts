import { Apps } from "../utils/enums/Apps";
import { ClinicalTrialMatchResults } from "./apps/clinicalTrialMatchResults";
import { GuidelinesPage } from "./apps/guidelinesPage";
import Page from "./page";

export class AppPatientMenu extends Page {
    get component() { return this.browser.$('app-patient-menu') }

    get appsButtons() { return this.component.$$('.label') }

    public goToApp(app: Apps, hasResults = false): void {
        switch (app) {
            case Apps.CTM:
                this.goToCTM(hasResults);
                break;
            case Apps.GDL:
                this.goToGDL(hasResults);
                break;
            case Apps.PS:
                this.goToPS(hasResults)
                break;
            default:
                break;
        }
    }

    private goToPS(hasResults: boolean): void {
        this.click(this.appsButtons[1]);
        this.switchOnCurrentFrame();
    }

    private goToGDL(hasResults: boolean): void {
        this.click(this.appsButtons[2]);
        this.switchOnCurrentFrame();
        new GuidelinesPage(this.browser).component.waitForDisplayed();
    }

    private goToCTM(hasResults: boolean): void {
        this.click(this.appsButtons[0]);
        this.switchOnCurrentFrame();
        const clinicalTrialMatchResults = new ClinicalTrialMatchResults(this.browser);

        clinicalTrialMatchResults.component.waitForDisplayed();

        if (hasResults) {
            expect(clinicalTrialMatchResults.matchList[0]).toBeDisplayed();
        } else {
            expect(clinicalTrialMatchResults.emptyState).toBeDisplayed();
        }
    }
}