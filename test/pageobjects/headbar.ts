import { Apps } from "../utils/enums/Apps";
import Page from "./page";
import { ClinicalTrialMatchResults } from "./apps/clinicalTrialMatchResults";
import { AppPatientSearch } from "./appPatientSearch";
import { GuidelinesPage } from "./apps/guidelinesPage";
export class Headbar extends Page {
    get component() { return this.browser.$('app-headbar') }
    get searchButton() { return this.component.$('.headbar__search-button') }
    get userName() { return this.component.$('.headbar__user-name') }
    get logoutButton() { return this.component.$('[data-e2e-label="headbar-dropdown-logout"]') }

    get appsForm() { return this.browser.$('[data-e2e-label="headbar-nav-apps"]') }
    get apps() { return this.browser.$$('nap-launchpad-button button div.icon') }

    public isUserNameDisplayed(): boolean {
        return this.userName.isDisplayed()
    }

    public getAppsTextValue(): string {
        return this.getText(this.appsForm);
    }

    public searchPatient(name: string, fullText: any = null): void {
        this.click(this.searchButton);
        new AppPatientSearch(this.browser).select(name, fullText);
    }

    public logout(): void {
        browser.switchToParentFrame();
        this.click(this.userName);
        this.click(this.logoutButton);
        this.component.waitForDisplayed({ reverse: true });
    }

    public goToApp(app: Apps, hasResults = false): void {
        this.click(this.appsForm);
        switch (app) {
            case Apps.CTM:
                this.goToCTM(hasResults);
                break;
            case Apps.GDL:
                this.goToGDL(hasResults);
                break;
            case Apps.PS:
                this.goToPS(hasResults)
                break;
            default:
                break;
        }
    }

    private goToCTM(hasResults: boolean): void {
        this.click(this.apps[0]);
        this.switchOnCurrentFrame();

        const clinicalTrialMatchResults = new ClinicalTrialMatchResults(this.browser);
        clinicalTrialMatchResults.component.waitForDisplayed();

        if (hasResults) {
            expect(clinicalTrialMatchResults.matchList[0]).toBeDisplayed();
        } else {
            expect(clinicalTrialMatchResults.emptyState).toBeDisplayed();
        }
    }

    private goToPS(hasResults: boolean): void {
        this.click(this.apps[1]);
        this.switchOnCurrentFrame();
    }

    private goToGDL(hasResults: boolean): void {
        this.click(this.apps[2]);
        this.switchOnCurrentFrame();
        new GuidelinesPage(this.browser).component.waitForDisplayed();
    }
}
