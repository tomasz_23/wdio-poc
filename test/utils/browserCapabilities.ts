import { Helper } from "./helper";

export class BrowserCapabilities {
    private static multisession = false;

    public static getBrowserCapabilities(browser: string, downloadsPath: any): any {
        this.multisession = Helper.isMultisession();
        return this.multisession ? this.getMultiSession(browser, downloadsPath) : this.getSingleSession(browser, downloadsPath);
    }

    private static getBrowser(browser: string, downloadsPath: any): any {
        return { capabilities: this.getSingleCapability(browser, downloadsPath) };
    }

    private static getMultiSession(browser: string, downloadsPath: any): any {
        return { browser_1: this.getBrowser(browser, downloadsPath), browser_2: this.getBrowser(browser, downloadsPath) };
    }

    private static getSingleSession(browserSelected: string, downloadsPath: any): any {
        return [this.getSingleCapability(browserSelected, downloadsPath)];
    }

    private static getSingleCapability(browser: string, downloadsPath: any): any {
        return {
            browserName: browser,
            acceptInsecureCerts: true,
            'goog:chromeOptions': {
                args: [
                    // '--headless',
                    '--no-sandbox',
                    '--disable-gpu',
                    '--window-size=1920,1080',
                    '--disable-web-security',
                    '--disable-dev-shm-usage'
                ],
                prefs: {
                    'safebrowsing.enabled': false,
                    'safebrowsing.disable_download_protection': true,
                    "download": {
                        "profile.default_conent_settings.popups": 0,
                        "prompt_for_download": false,
                        "directory_upgrade": true,
                        "default_directory": downloadsPath
                    }
                }
            }
        };
    }
}