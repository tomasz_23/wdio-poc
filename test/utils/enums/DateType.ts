export enum DateType {
    HOUR, DAY, MONTH, YEAR
}