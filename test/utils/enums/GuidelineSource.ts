export enum GuidelineSource {
    NCCN = 'NCCN Guidelines®',
    CLINICAL_PATHWAYS = 'Clinical Pathways'
}