import { Browser } from "@wdio/types/build/Clients";

export class Helper {

    static getParam(name: string): any {
        const param = process.argv.filter((element) => element.match(name))[0];
        return param ? param.split('=')[1] : null;
    }

    static async pdfContains(fileName: string, expectedText: string): Promise<boolean> {
        const pdfLib = require('pdf-parse');
        const path = require('path');
        const pathToFile = path.join(__dirname, `../../downloads/${fileName}.pdf`)

        return await pdfLib(pathToFile).then(pdf => {
            return pdf.text.includes(expectedText);
        });
    }

    static isMultisession(): boolean {
        return this.getParam('multisession') == 'true' ? true : false;
    }

    static getDefaultBrowser(): Browser {
        return browser;
    }

    private static readParamsFromCli(): any {
        const paramsPair = process.argv.slice(3).filter(it => {
            return it.startsWith('--D');
        });

        const params: any = {};
        paramsPair.forEach(pair => {
            const parts = pair.split('=');
            const name = parts[0].trim().replace('--D', '');
            const value = parts[1] && parts[1].trim() || true;
            params[name] = value;
        });

        return params;
    }

    public static getEnvFile(): any {
        const params = this.readParamsFromCli();
        try {
            return require(`../configs/${params.env.toLocaleString().toLowerCase()}.js`);
        } catch (error) {
            throw new Error(`Missing or wrong params --Denv : ${error}`);
        }
    }

    static savePrintPdf(name: string): void {
        browser.pause(15000);
        const nodeKeySender = require('node-key-sender');
        nodeKeySender.sendKey('enter');
        browser.pause(5000);
        nodeKeySender.sendText(name);
        browser.pause(2000);
        nodeKeySender.sendKey('enter');
        browser.pause(5000);
    }
}