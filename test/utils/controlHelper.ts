import { Helper } from "./helper";

export class ControlHelper {
    readonly conf = Helper.getEnvFile();
    token: Promise<string>;
    user: any;

    constructor(restHelper = null) {
        if (restHelper) {
            this.token = restHelper.token.then(value => value);
            this.user = restHelper.finalUser;
        }
    }

    public async runTumorBoard(browser_local = Helper.getDefaultBrowser()): Promise<void> {
        browser_local.setCookies([
            { name: 'access_token', value: await this.getToken(), path: '/', domain: '.platform.navify.com' }
        ]);
        browser_local.url(this.getUrl());
        await browser.pause(3000);
        browser.refresh();
        await browser.pause(15000);
    }

    private getUrl(): string {
        return this.conf.baseUrl.replace('su-appsqa', this.user.tenantAlias);
    }

    private getToken(): Promise<string> {
        return this.token.then(token => token.slice(7));
    }
}