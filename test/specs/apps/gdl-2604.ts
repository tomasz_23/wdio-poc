import { PatientCalls } from "../../api/patientCalls";
import { RestHelper } from "../../api/restHelper";
import { AppPatientMenu } from "../../pageobjects/appPatientMenu";
import { AnalyticsPage } from "../../pageobjects/apps/analyticsPage";
import { GuidelinesPage } from "../../pageobjects/apps/guidelinesPage";
import { Headbar } from "../../pageobjects/headbar";
import { ControlHelper } from "../../utils/controlHelper";
import { Apps } from "../../utils/enums/Apps";
import { Customization } from "../../utils/enums/Customization";
import { Guideline } from "../../utils/enums/Guideline";
import { GuidelineCancerType } from "../../utils/enums/GuidelineCancerType";
import { GuidelineSource } from "../../utils/enums/GuidelineSource";
import { GuidelineStartingPoint } from "../../utils/enums/GuidelineStartingPoint";
import { PatientHelper } from "../../utils/patientHelper";

describe('@GDL-2604 Show Clinical pathways by cancer type', () => {
    const restHelper = new RestHelper();
    const user = new ControlHelper(restHelper);
    const patientCalls = new PatientCalls(restHelper);
    const patientData = PatientHelper.getNewPatientData();

    const headbar = new Headbar();
    const analyticsPage = new AnalyticsPage();
    const guidelinesPage = new GuidelinesPage();
    const appPatientMenu = new AppPatientMenu();

    let patientUUID: number;
    let amountOfCancerType: number;

    beforeAll(async () => {
        patientUUID = await patientCalls.createPatient(patientData);
        await user.runTumorBoard();
    })

    it('Step 01 - Log in as Default user.', () => {
        expect(headbar.userName).toBeDisplayed();
    });

    it('Step 02 - Launch Guidelines from "Apps" menu.', () => {
        headbar.goToApp(Apps.GDL);
    });

    it('Step 03 - Click "help & more" link and select "Analytics".', () => {
        guidelinesPage.goToAnalytics();
    });

    it('Step 04 - Check "Clinical pathways by cancer type:" section.', () => {
        expect(analyticsPage.barChart).toBeDisplayed();
        expect(analyticsPage.gdlAnalyticsPathwaysByCancerListElements).toBeDisplayed();
    });

    it('Step 05 - Hover over dark blue Breast Cancer chart.', () => {
        analyticsPage.hoverOnColumn('Breast Cancer', Customization.WITHOUT);
        amountOfCancerType = parseInt(analyticsPage.getAmountOfCancerTypeFromTooltip());
    });

    it('Step 06 - Launch Guideliness from Patient`s A perspective and create pathway for Breast Cancer.', () => {
        browser.switchToParentFrame();
        headbar.searchPatient(patientData.mrn);
        appPatientMenu.goToApp(Apps.GDL);
        guidelinesPage.fillDropDownList(Guideline.GuidelineSource, GuidelineSource.NCCN);
        guidelinesPage.fillDropDownList(Guideline.GuidelineCancerType, GuidelineCancerType.BREAST_CANCER);
        guidelinesPage.fillDropDownList(Guideline.GuidelineStartingPoint, GuidelineStartingPoint.CLINICAL_STAGE_WORKUP);
        guidelinesPage.selectAndGetNode();
    });

    it('Step 07 - Open "Analytics" again and change "Select Cancer Type:" dropdown value to "Breast Cancer".', () => {
        guidelinesPage.goToAnalytics();
        expect(analyticsPage.barChart).toBeDisplayed();
        expect(analyticsPage.gdlAnalyticsPathwaysByCancerListElements).toBeDisplayed();
    });

    it('Step 08 - Hover over dark blue bar chart for Breast Cancer.', () => {
        analyticsPage.hoverOnColumn('Breast Cancer', Customization.WITHOUT);
        expect(amountOfCancerType + 1).toEqual(parseInt(analyticsPage.getAmountOfCancerTypeFromTooltip()));
    });

    it('Step 09 - Log out from the application.', () => {
        headbar.logout();
    });

    afterAll(async () => {
        await patientCalls.deletePatient(patientUUID);
    })
});