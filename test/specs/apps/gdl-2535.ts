import { PatientCalls } from "../../api/patientCalls";
import { RestHelper } from "../../api/restHelper";
import { AppPatientMenu } from "../../pageobjects/appPatientMenu";
import { GuidelinesPage } from "../../pageobjects/apps/guidelinesPage";
import { Headbar } from "../../pageobjects/headbar";
import { ControlHelper } from "../../utils/controlHelper";
import { Apps } from "../../utils/enums/Apps";
import { Guideline } from "../../utils/enums/Guideline";
import { GuidelineCancerType } from "../../utils/enums/GuidelineCancerType";
import { GuidelineSource } from "../../utils/enums/GuidelineSource";
import { GuidelineStartingPoint } from "../../utils/enums/GuidelineStartingPoint";
import { Helper } from "../../utils/helper";
import { PatientHelper } from "../../utils/patientHelper";
describe('@GDL-2535 Bullets Points in PDF output - patients perspective', () => {
    const restHelper = new RestHelper();
    const user = new ControlHelper(restHelper);
    const patientCalls = new PatientCalls(restHelper);
    const patientData = PatientHelper.getNewPatientData();

    const headbar = new Headbar();
    const guidelinesPage = new GuidelinesPage();
    const appPatientMenu = new AppPatientMenu();

    const pdfFileName = 'testPdf';

    let patientUUID: number;
    let selectedNode: string;

    beforeAll(async () => {
        patientUUID = await patientCalls.createPatient(patientData);
        // await patientCalls.addTumorInformation(patientUUID, false, true, true);
        await user.runTumorBoard();
    })

    it('Step 01 - Log in as Default user.', () => {
        expect(headbar.userName).toBeDisplayed();
    });

    it('Step 02 - Click “Patients” tab and click on patient’s name from preconditions.', () => {
        headbar.searchPatient(patientData.mrn);
    });

    it('Step 03 - Click "Guidelines" app icon. Select "NCCN Guidelines®" as guideline source.', () => {
        appPatientMenu.goToApp(Apps.GDL, true);

        guidelinesPage.fillDropDownList(Guideline.GuidelineSource, GuidelineSource.NCCN);
        guidelinesPage.fillDropDownList(Guideline.GuidelineCancerType, GuidelineCancerType.BREAST_CANCER);
        guidelinesPage.fillDropDownList(Guideline.GuidelineStartingPoint, GuidelineStartingPoint.CLINICAL_STAGE_WORKUP);
    });

    it('Step 04 - Switch view to "Tree View".', () => {
        guidelinesPage.selectTreeView();
    });

    it('Step 05 - Click print icon.', () => {
        selectedNode = guidelinesPage.selectAndGetNode();
        guidelinesPage.print.click();
        Helper.savePrintPdf(pdfFileName);
    });

    it('Step 06 - Check pdf.', async () => {
        expect(await Helper.pdfContains(pdfFileName, selectedNode)).toBeTruthy();
    });

    it('Step 07 - Log out from the application.', () => {
        headbar.logout();
    });

    afterAll(async () => {
        await patientCalls.deletePatient(patientUUID);
    })
});