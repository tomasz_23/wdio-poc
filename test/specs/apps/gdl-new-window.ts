import { PatientCalls } from "../../api/patientCalls";
import { RestHelper } from "../../api/restHelper";
import { Headbar } from "../../pageobjects/headbar";
import { ControlHelper } from "../../utils/controlHelper";
import { Helper } from "../../utils/helper";
import { PatientHelper } from "../../utils/patientHelper";

describe('@GDL-2535 new windows', () => {
    const users = Helper.getEnvFile().params;
    const restHelper = new RestHelper(users.login);
    const restHelper2 = new RestHelper(users.loginUCSF)
    const patientCalls = new PatientCalls(restHelper);

    let headbar = new Headbar();

    const user = new ControlHelper(restHelper);
    const user2 = new ControlHelper(restHelper2);

    const patientData = PatientHelper.getNewPatientData();

    let patientUUID: number;

    const browsers = [];

    beforeAll(async () => {
        patientUUID = await patientCalls.createPatient(patientData);
        user.runTumorBoard(browser_1);
        user2.runTumorBoard(browser_2);

        browsers.push(browser_1);
        browsers.push(browser_2);
    })

    it('Step 01 - Log in as User A on one browser page, and log in as User B on the other one.', () => {
        browsers.forEach(browser => {
            headbar = new Headbar(browser);
            headbar.component.waitForDisplayed();
            expect(headbar.userName.isDisplayed()).toBe(true);
        })
    });

    it('Step 02 - Navigate to Patients tab and click on the patient from Preconditions (for User A and B).', () => {
        browsers.forEach(browser => {
            headbar = new Headbar(browser);
            headbar.searchPatient(patientData.mrn);
        })
    });

    afterAll(async () => {
        await patientCalls.deletePatient(patientUUID);
    })
});