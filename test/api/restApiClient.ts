import logger from '@wdio/logger';
import axios from "axios";
import { RequestType } from "../utils/enums/RequestType";
import { RestHelper } from "./restHelper";

export class RestApiClient {
    public finalUser: any;
    private logger: any = logger('[RestApiClient]');
    private host: any;
    private restHelper: RestHelper;
    conf: any;

    constructor(restHelper: RestHelper) {
        this.finalUser = restHelper.finalUser;
        this.conf = restHelper.conf
        this.host = this.conf.baseUrl.replace(this.conf.params.login.tenantAlias, restHelper.finalUser.tenantAlias)
        this.restHelper = restHelper;
    }

    private async getHeaders(): Promise<any> {
        const token: any = await this.restHelper.token;
        return {
            'X-Navify-Tenant': this.restHelper.finalUser.tenantAlias,
            'Authorization': token
        };
    }

    public async get(endpoint: string): Promise<any> {
        return await this.getCall(endpoint, RequestType.GET);
    }

    public async put(endpoint: string, json: any): Promise<any> {
        return await this.getCall(endpoint, RequestType.PUT, json);
    }

    public async post(endpoint: string, json: any): Promise<any> {
        return await this.getCall(endpoint, RequestType.POST, json);
    }

    public async delete(endpoint: string): Promise<any> {
        return await this.getCall(endpoint, RequestType.DELETE);
    }

    private async getCall(endpoint: string, requestType: RequestType, json: any = null): Promise<any> {
        const finalUrl = this.host + endpoint;
        let err: any;
        const response = await (axios({
            method: requestType,
            url: finalUrl,
            headers: await this.getHeaders(),
            data: json
        })).catch(error => err = error);

        if (err) {
            this.logger.error(`[${requestType.toUpperCase()}] Fail call for "${endpoint}"`);

            this.logger.error('REQUEST');
            this.logger.error(response.config.data);

            this.logger.error('RESPONSE');
            this.logger.error(response.response.status);
            this.logger.error(response.response.data);
        } else {
            this.logger.info(`[${requestType.toUpperCase()}] Success call for "${endpoint}"`);
        }

        return response;
    }
}