import { DateHelper } from "../utils/dateHelper";
import { RestApiClient } from "./restApiClient";
import { RestHelper } from "./restHelper";
import { CancerInfoHelper } from "../utils/cancerInfoHelper";

export class PatientCalls {
    readonly patientURL = 'tumor-board/api/v1/patient/';

    restHelper: RestHelper
    restApiClient: RestApiClient

    constructor(restHelper: RestHelper) {
        this.restHelper = restHelper;
        this.restApiClient = new RestApiClient(restHelper);
    }

    async createPatient(patientData: any, facilityID: any = null): Promise<any> {
        const facility = facilityID ? facilityID : this.restApiClient.finalUser.facilityID;

        const preparedJSON = {
            "firstName": patientData.firstName,
            "lastName": patientData.lastName,
            "gender": patientData.gender.substring(0, 1),
            "dateOfBirth": DateHelper.getFormattedDate(patientData.dateOfBirth, 'MMDDYYYY', 'YYYY-MM-DD'),
            "facility": facility,
            "patientIdentifiers":
                [
                    {
                        "system": "http://dip.roche.com/context-identifier/impacted-patient-identifier/assigner-UHC/UEI",
                        "facility": facility,
                        "value": patientData.mrn,
                    },
                    {
                        "system": "http://dip.roche.com/context-identifier/impacted-patient-identifier/assigner-UHC/UHC",
                        "facility": facility,
                        "value": patientData.mrn
                    }
                ]
        };
        return (await this.restApiClient.post(this.patientURL, preparedJSON)).data.uuid;
    }

    private async getDictionariesFromOrganization(facilityID: string): Promise<any> {
        const response = await this.restApiClient.get("tumor-board/api/v2/organizations/" + facilityID);
        return new RegExp(response.data).test(".*\"mappedDictionaryFields\":\\[{.*}\\].*");
    }

    private async getTumorInformationVersionId(patientUUID: number): Promise<string> {
        const response = await this.restApiClient.get(this.patientURL + patientUUID + "/tumor-info");
        return response.data.versionId;
    }

    public async addTumorInformation(patientUUID: number, hasDictionaries = false, tumorType: any = null, tumorLocation: any = null, facilityID: string = this.restApiClient.finalUser.facilityID, tumorData: any = null): any {
        tumorData = tumorData == null ? CancerInfoHelper.getTumorInformationContent() : tumorData;
        const dictionaries: any = hasDictionaries ? hasDictionaries : await this.getDictionariesFromOrganization(facilityID);

        const preparedTumorTypeJSON: any = {};
        preparedTumorTypeJSON.versionId = await this.getTumorInformationVersionId(patientUUID);

        let type: any;
        let location: any;

        if (dictionaries === null) {
            if (tumorType) {
                type = {
                    "type": tumorData.type
                };
            }

            if (tumorLocation) {
                location = {
                    "location": tumorData.location,
                    "description": tumorData.locationDescription
                };
            }

        } else {
            if (tumorType) {
                type = {
                    "codedTerm": {
                        "termCode": tumorData.termCodeType,
                        "termDescription": tumorData.type,
                        "system": tumorData.system,
                        "version": tumorData.version,
                    }
                };
            }

            if (tumorLocation) {
                location = {
                    "location": tumorData.location,
                    "description": tumorData.locationDescription,
                    "codedTerm": {
                        "termCode": tumorData.termCodeLocation,
                        "termDescription": tumorData.location,
                        "system": tumorData.system,
                        "version": tumorData.version,
                    }
                }
            }
        }

        preparedTumorTypeJSON.type = type;
        preparedTumorTypeJSON.location = location;
        return await this.restApiClient.put(this.patientURL + patientUUID + '/tumor-info', preparedTumorTypeJSON);
    }

    public async deletePatient(uuid: number): Promise<void> {
        await this.restApiClient.delete(this.patientURL + uuid);
    }
}
